@extends('layouts.app')

@section('content')
    <div class="container">
        @if(null !== session('carSold'))
            @if(session('carSold'))
                <div class="alert alert-success" role="alert">
                    Car sell confirmed
                </div>
            @endif
        @endif
        @component('components.carDealer.usedCars.searchBar', ['routeName' => $searchRouteName])
        @endcomponent
        
        <div class="card">
        @component('components.carDealer.usedCars.sellRequestTable', ['sellRequests' => $sellRequests, 'history' => isset($history) ? true : false])
        @endcomponent
        </div>
    </div>
@endsection
