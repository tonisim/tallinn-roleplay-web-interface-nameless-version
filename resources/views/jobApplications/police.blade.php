@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header justify-content-center"><b>{{__('headers.police_job_application')}}</b></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('jobApplicationStore') }}">
                            @csrf
                            <input type="hidden" name="job" value="police">
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label for="discordName">{{ __('jobApplications.discordName') }}</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('discordName') ? ' is-invalid' : '' }}"
                                           id="discordName" placeholder="{{__('jobApplications.discordName')}}"
                                           name="discordName" value="{{ old('discordName') }}" required autofocus>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="age">{{ __('jobApplications.age') }}</label>
                                    <input type="number"
                                           class="form-control {{ $errors->has('age') ? ' is-invalid' : '' }}"
                                           id="age" placeholder="{{ __('jobApplications.age') }}" name="age"
                                           value="{{ old('age') }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-4">
                                    <p>{{ __('jobApplications.sex') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sex"
                                               id="sex" value="{{ __('jobApplications.man') }}">
                                        <label class="form-check-label" for="sexMan">
                                            {{ __('jobApplications.man') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sex"
                                               id="sex" value="{{ __('jobApplications.woman') }}" checked>
                                        <label class="form-check-label" for="sexWoman">
                                            {{ __('jobApplications.woman') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <p>{{ __('jobApplications.gunlicense') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gunlicense"
                                               id="gunlicense" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="gunLicenseYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="gunlicense"
                                               id="gunlicense" value="{{ __('jobApplications.false') }}" checked>
                                        <label class="form-check-label" for="gunLicenseNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <p>{{ __('jobApplications.vehiclelicenses') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="vehiclelicenses"
                                               id="vehiclelicensesYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="vehiclelicensesYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="vehiclelicenses"
                                               id="vehiclelicensesNo" value="{{ __('jobApplications.false') }}" checked>
                                        <label class="form-check-label" for="vehiclelicensesNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label for="wanttopolice">{{ __('jobApplications.wanttopolice') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('wanttopolice') ? ' is-invalid' : '' }}"
                                        id="wanttopolice" placeholder="{{ __('jobApplications.wanttopolice') }}" name="wanttopolice"
                                        required autofocus>{{ old('wanttopolice') }}</textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label
                                        for="policepriority">{{ __('jobApplications.policepriority') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('policepriority') ? ' is-invalid' : '' }}"
                                        id="policepriority" placeholder="{{ __('jobApplications.policepriority') }}"
                                        name="policepriority" required
                                        autofocus>{{ old('policepriority') }}</textarea>
                                </div>
                            </div>

                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label
                                        for="career">{{ __('jobApplications.career') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('career') ? ' is-invalid' : '' }}"
                                        id="career" placeholder="{{ __('jobApplications.career') }}"
                                        name="career" required autofocus>
                                            {{ old('career') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label
                                        for="policeactivityeveryday">{{ __('jobApplications.policeactivityeveryday') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('policeactivityeveryday') ? ' is-invalid' : '' }}"
                                        id="policeactivityeveryday" placeholder="{{ __('jobApplications.policeactivityeveryday') }}"
                                        name="policeactivityeveryday" required autofocus>
                                            {{ old('policeactivityeveryday') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label for="whatyoucangivetopolicestation">{{ __('jobApplications.whatyoucangivetopolicestation') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('whatyoucangivetopolicestation') ? ' is-invalid' : '' }}"
                                        id="whatyoucangivetopolicestation" placeholder="{{ __('jobApplications.whatyoucangivetopolicestation') }}"
                                        name="whatyoucangivetopolicestation" required autofocus>
                                            {{ old('whatyoucangivetopolicestation') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label for="previousexperience">{{ __('jobApplications.previousexperience') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('previousexperience') ? ' is-invalid' : '' }}"
                                        id="previousexperience" placeholder="{{ __('jobApplications.previousexperience') }}"
                                        name="previousexperience" required autofocus>
                                            {{ old('previousexperience') }}
                                    </textarea>
                                </div>
                            </div>

                            <input type="submit" class="btn btn-success" id="confirmButton" onclick="overlayOn()"
                                   value="{{__('buttons.submit_application')}}" disabled>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload = function () {
        const discordNameField = document.getElementById('discordName');
        const ageField = document.getElementById('age');
        const sex = document.getElementById('sex');
        const wanttopolice = document.getElementById('wanttopolice');
        const policePriority = document.getElementById('policepriority');
        const career = document.getElementById('career');
        const policeactivityeveryday = document.getElementById('policeactivityeveryday');
        const whatyoucangivetopolicestation = document.getElementById('whatyoucangivetopolicestation');
        const previousexperience = document.getElementById('previousexperience');
        const confirmButton = document.getElementById('confirmButton');

        let elementArray = [
            discordNameField,
            ageField,
            sex,
            wanttopolice,
            policePriority,
            career,
            policeactivityeveryday,
            whatyoucangivetopolicestation,
            previousexperience,
        ];

        elementArray.forEach(function (element) {
            element.addEventListener('keyup', function (event) {
                let isValidDiscordNameField = discordNameField.checkValidity();
                let isValidAgeField = ageField.checkValidity();
                let isValidSex = sex.checkValidity();
                let isValidwanttopolice = wanttopolice.checkValidity();
                let isValidpolicePriority = policePriority.checkValidity();
                let isValidcareer = career.checkValidity();
                let isValidpoliceactivityeveryday = policeactivityeveryday.checkValidity();
                let isValidwhatyoucangivetopolicestation = whatyoucangivetopolicestation.checkValidity();
                let isValidpreviousexperience = previousexperience.checkValidity();

                if (
                    isValidDiscordNameField
                    && isValidAgeField
                    && isValidSex
                    && isValidwanttopolice
                    && isValidpolicePriority
                    && isValidcareer
                    && isValidpoliceactivityeveryday
                    && isValidwhatyoucangivetopolicestation
                    && isValidpreviousexperience
                ) {
                    confirmButton.disabled = false;
                } else {
                    confirmButton.disabled = true;
                }
            });
        });
    }
</script>
