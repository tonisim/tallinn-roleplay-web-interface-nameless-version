@extends('user.layout.layout')
@section('userBody')
    <div class="row row-eq-height mt-5 d-flex" style="height: 160px">
        <div class="col-md-6 mouse-over h-100" onclick="showProfileUpload()">
            @component('components.userDataBox', ['user' => $userData])
            @endcomponent
        </div>
        <div class="col-md-6 h-100">
            @component('components.moneyBox', ['data' => $userData])
            @endcomponent
        </div>
    </div>
    <div class="row row-eq-height mt-5 d-flex" style="height: 250px;">
        <div class="col-md-6">
          @component('components.billsBox', ['billsExtraData' => $billsExtraData, 'bills' => $bills, 'userData' => $userData])
          @endcomponent
        </div>
        <div class="col-md-6">
            @component('components.licenseBox', ['licenses' => $licenses, 'identifier' => $userData->identifier])
            @endcomponent
        </div>
    </div>
    <div class="row row-eq-height mt-5 d-flex" style="height: 250px;">
        @if(isset($userData->job_grade) and $userData->job_grade >= 90)
            <div class="col-md-6">
                @component('components.companyBox', ['companyMoney' => $companyInfo['money'], 'companyName' => $companyInfo['name']])
                @endcomponent
            </div>
        @endif
    </div>


    @if(in_array($userData->job, \App\Http\Controllers\PoliceController::ALLOWED_JOBS))
        <button class="mt-3 btn btn-success" data-toggle="collapse"
                href="#officerCallSignForm" aria-expanded="false"
                aria-controls="officerCallSignForm">
            {{__('buttons.set_call_sign')}}
        </button>
        <div class="card mt-3 collapse" id="officerCallSignForm">
            <div class="card-body">
                <form method="POST" action="{{route('setCallSign')}}"
                      class="mt-3 mb-3" onsubmit="overlayOn()">
                    @csrf
                    <input type="hidden" name="userIdentifier" value="{{$userData->identifier}}">
                    <label for="callSign">{{ __('forms.call_sign') }}:</label>
                    <input type="text" id="callSign" name="callSign" class="form-control"
                           placeholder="{{ __('forms.call_sign') }}" required>
                    <button type="submit" class="btn btn-info mt-3"
                            onclick="overlayOn()" id="confirmButtonChangeName">
                        {{__('buttons.confirm')}}
                    </button>
                </form>
            </div>
        </div>
    @endif
@endsection
<script>

    /**
     * @returns {boolean}
     * @constructor
     */
    function ConfirmSell() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }
</script>
