<div class="card-header">
    <b>{{__('headers.user_cars')}}</b>
    <span
        class="badge badge-pill badge-secondary">{{\App\OwnedCar::where('owner', $userData->identifier)->get()->count()}}</span>
</div>
<div class="card-body" id="carsBody">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif
    @if(null !== session('carSellRequestStatus'))
        @if(session('carSellRequestStatus'))
            <div class="alert alert-success" role="alert">
                {{__('texts.car_sell_request_created_go_see_car_dealer_who_can_confirm_your_sell_request')}}
            </div>
        @else
            <div class="alert alert-danger" role="alert">
                {{__('texts.car_sell_request_creation_failed_try_again')}}
            </div>
        @endif
    @endif
    @if(null !== session('carSellRequestExist'))
        @if(session('carSellRequestExist'))
            <div class="alert alert-danger" role="alert">
                {{__('texts.car_sell_request_already_created_go_see_car_dealer_who_can_confirm_your_sell_request')}}
            </div>
        @endif
    @endif
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">{{__('tables.number_plate')}}</th>
            <th>{{__('tables.model_name')}}</th>
            <th>{{__('tables.is_insured')}}</th>
            <th>{{__('tables.is_stolen')}}</th>
            <th>{{__('tables.insurance_end_date')}}</th>
            <th>{{__('tables.sell_car')}}</th>
            <th>{{__('tables.sell_request_nr')}}</th>
            <th>{{__('tables.actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
            <tr>
                <td>{{$car->plate}}</td>
                <td>
                    @isset(\App\CarModel::where('code', json_decode($car->vehicle)->model)->first()->name)
                        {{\App\CarModel::where('code', json_decode($car->vehicle)->model)->first()->name}}
                    @endisset
                </td>

                <td>
                    @if(
                            isset(\App\CarInsurance::where('plate', $car->plate)->first()->is_insured)
                            and isset(\App\CarInsurance::where('plate', $car->plate)->first()->end_time)
                            and \App\CarInsurance::where('plate', $car->plate)->first()->is_insured
                            and new DateTime(\App\CarInsurance::where('plate', $car->plate)->first()->end_time) > new DateTime()
                        )
                        <div class="text-success">Olemas</div>
                    @else
                        <div class="text-danger">Puudub</div>
                    @endif
                </td>
                <td>
                    @if(\App\StolenCar::where('plate', $car->plate)->where('is_stolen', true)->first())
                        <div class="text-danger">Jah</div>
                    @else
                        Ei
                    @endif
                </td>
                <td>
                    @if(isset(\App\CarInsurance::where('plate', $car->plate)->first()->end_time))
                        {{\App\CarInsurance::where('plate', $car->plate)->first()->end_time}}
                    @endif
                </td>
                <td>@if($car->finance > 0)
                        <p>Hetkel pole võimalik müüa</p>
                    @else
                        <button id="sellCar" class="btn btn-info" data-toggle="collapse"
                                href="#plate-{{str_replace(' ', '', $car->plate)}}"
                                aria-expanded="false"
                                aria-controls="plate-{{str_replace(' ', '', $car->plate)}}">
                            {{__('buttons.sell_car')}}
                        </button>

                        <form id="plate-{{str_replace(' ', '',$car->plate)}}"
                              method="POST" action="{{route('sellCar')}}"
                              class="collapse mt-3"
                              onsubmit="return ConfirmSell()">
                            @csrf
                            <input type="hidden" name="carPlate" value="{{$car->plate}}">
                            <input type="hidden" name="sellerId" value="{{$userData->identifier}}">
                            <label for="buyerId">{{__('forms.buyer_name')}}:</label>
                            <select class="form-control" id="buyerId" name="buyerId" required>
                                <option></option>
                                @foreach(\App\User::orderBy('name', 'ASC')->get() as $user)
                                    <option value="{{$user->identifier}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-info mt-3" onclick="overlayOn()">
                                {{__('buttons.confirm_sell')}}
                            </button>
                        </form>
                    @endif
                </td>
                <td class="text-center font-weight-bold">
                    @if($carSellRequests->count() > 0)
                        @foreach($carSellRequests as $carSellRequest)
                            @if ($car->plate === $carSellRequest->plate)
                                #{{$carSellRequest->id}}
                            @endif
                        @endforeach
                    @endif
                </td>
                <td>
                    @if($carSellRequests->count() > 0)
                        @foreach($carSellRequests as $carSellRequest)
                            @if ($car->plate === $carSellRequest->plate)
                                <form method="POST" action="{{route('cancelCarSell')}}"
                                      class="m-0" onsubmit="return ConfirmSell()">
                                    @csrf
                                    <input type="hidden" name="sellRequestId"
                                           value="{{$carSellRequest->id}}">
                                    <button type="submit" class="btn btn-danger btn-block" onclick="overlayOn()">
                                        {{__('buttons.cancel_sell_request')}} #{{$carSellRequest->id}}
                                    </button>
                                </form>
                            @endif
                        @endforeach
                    @endif
                    @if(\App\StolenCar::where('plate', $car->plate)->where('is_stolen', true)->first())
                        <form method="POST" action="{{route('homeUnDeclareCarAsStolen')}}"
                              class="m-0 mt-1" onsubmit="return ConfirmSell()">
                            @csrf
                            <input type="hidden" name="plate" value="{{$car->plate}}">
                            <button type="submit"
                                    class="btn btn-warning btn-block">{{__('buttons.car_is_not_stolen_any_more')}}</button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach
        @foreach($confiscatedCars as $confiscatedCar)
            <tr>
                <td>
                    {{$confiscatedCar->carPlate}}
                </td>
                <td>
                    Konfiskeeritud
                </td>
                <td>
                    @if(
                          isset(\App\CarInsurance::where('plate', $confiscatedCar->carPlate)->first()->is_insured)
                          and isset(\App\CarInsurance::where('plate', $confiscatedCar->carPlate)->first()->end_time)
                          and \App\CarInsurance::where('plate', $confiscatedCar->carPlate)->first()->is_insured
                          and new DateTime(\App\CarInsurance::where('plate', $confiscatedCar->carPlate)->first()->end_time) > new DateTime()
                      )
                        <div class="text-success">Olemas</div>
                    @else
                        <div class="text-danger">Puudub</div>
                    @endif
                </td>
                <td>

                </td>
                <td>
                    @if(isset(\App\CarInsurance::where('plate', $confiscatedCar->carPlate)->first()->end_time))
                        {{\App\CarInsurance::where('plate', $confiscatedCar->carPlate)->first()->end_time}}
                    @endif
                </td>
                <td>

                </td>
                <td>

                </td>
                <td>

                </td>
            </tr>



        @endforeach
        </tbody>
    </table>
</div>
