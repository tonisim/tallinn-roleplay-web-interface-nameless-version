@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <p><strong>{{__('texts.total_amount')}}:</strong> {{$total}}</p>
                <p><strong>{{__('texts.total_bills')}}: </strong> {{$count}}</p>
            </div>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th>{{__('tables.total')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($billsCollection as $userBilling)
                    <tr>
                        <td>{{$userBilling['characterName']}}</td>
                        <td>{{$userBilling['total']}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
