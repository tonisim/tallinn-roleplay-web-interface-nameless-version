@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <h1>{{$bailiff_site_header}}</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th>{{__('tables.total')}}</th>

                </tr>
                </thead>
                <tbody>
                @foreach($billsCollection as $userBilling)


                    <tr>
                        <th scope="row" data-toggle="collapse"
                            data-target="#accordion"
                            class="clickable">
                            {{$userBilling['characterName']}}
                        </th>
                        <th scope="row" data-toggle="collapse"
                            data-target="#accordion"
                            class="clickable">
                            {{$userBilling['total']}}
                        </th>


                    </tr>

                    <tr>
                        <td colspan="12">

                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

