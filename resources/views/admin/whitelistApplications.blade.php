@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.steam_id')}}</th>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th scope="col">{{__('tables.steam_name')}}</th>
                    <th scope="col">{{__('tables.last_rejection_reason')}}</th>
                    <th scope="col">{{__('tables.actions')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($whitelists as $whitelist)

                <tr>
                    <th scope="row" data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->user_identifier }}
                    </th>
                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->characterName }}
                    </td>
                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->steamName }}
                    </td>
                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->rejectionReason }}
                    </td>
                    <td class="click">
                        <form method="post" action="{{route('adminAcceptWhitelistApplication')}}"
                              class="d-inline-block">
                            @csrf
                            <input type="hidden" name="id" value="{{$whitelist->id}}">
                            <input class="btn btn-success" type="submit" value="{{__('buttons.accept')}}"
                                   onclick="overlayOn()">
                        </form>
                        <button id="reject" class="btn btn-danger" data-toggle="collapse"
                                href="#application-{{str_replace(' ', '', $whitelist->id)}}"
                                aria-expanded="false"
                                aria-controls="application-{{str_replace(' ', '', $whitelist->id)}}">
                            {{__('buttons.reject')}}
                        </button>
                        {{--<form id="application-{{str_replace(' ', '', $whitelist->id)}}" class="collapse mt-3"
                              method="post" action="{{route('adminRejectWhitelistApplication')}}">
                            @csrf
                            <input type="hidden" name="id" value="{{$whitelist->id}}">
                            <label for="reason">Reason:</label>
                            <textarea name="reason" id="reason" class="form-control"></textarea>
                            <input class="btn btn-info mt-3" type="submit" value="{{__('buttons.confirm')}}"
                                   onclick="overlayOn()">
                        </form>--}}
                    </td>
                </tr>
                <tr>
                    <td colspan="12">
                        <form id="application-{{str_replace(' ', '', $whitelist->id)}}" class="collapse mt-3"
                              method="post" action="{{route('adminRejectWhitelistApplication')}}">
                            @csrf
                            <input type="hidden" name="id" value="{{$whitelist->id}}">
                            <label for="reason">Reason:</label>
                            <textarea name="reason" id="reason" class="form-control"></textarea>
                            <input class="btn btn-info mt-3" type="submit" value="{{__('buttons.confirm')}}"
                                   onclick="overlayOn()">
                        </form>
                    </td>
                </tr>
                @if($whitelist->extra_questions != "null" and $whitelist->extra_questions != null)
                    <tr>
                        <td colspan="12">
                            <div id="accordion{{$whitelist->id}}" class="collapse">
                                @php
                                    $extraQuestions = json_decode($whitelist->extra_questions)
                                @endphp
                                @foreach($extraQuestions as $question => $answer)
                                    <p><strong>{{__('forms.' . $question)}}</strong></p>
                                    <p>{{$answer}}</p>

                                @endforeach
                            </div>
                        </td>
                    </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
