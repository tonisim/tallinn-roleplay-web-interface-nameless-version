@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.steam_name')}}</th>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th scope="col">{{__('tables.last_rejection_reason')}}</th>
                    <th scope="col">{{__('tables.whitelisted')}}</th>
                    <th scope="col">{{__('tables.updated')}}</th>
                    <th scope="col">{{__('tables.newTry')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($allwhitelists as $whitelist)

                <tr>
                    <th scope="row" data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->steamName }}
                    </th>
                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->characterName }}
                    </td>

                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->rejectionReason }}
                    </td>

                    <td>
                        @if($whitelist->isAccepted)
                            <i class="fas fa-check text-success"></i>
                        @else
                            <i class="fas fa-times text-danger"></i>

                    </td>
                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->updated_at }}
                    </td>
                    <td data-toggle="collapse" data-target="#accordion{{$whitelist->id}}" class="clickable">
                        {{ $whitelist->newTry }}
                    </td>
                    @endif
                </tr>

                    <tr>
                        <td colspan="12">
                            <div id="accordion{{$whitelist->id}}" class="collapse">
                                @php
                                    $whitelist=$extraQuestions = json_decode($whitelist->extra_questions)
                                @endphp
                                @foreach($extraQuestions as $question => $answer)
                                    <p><strong>{{__('forms.' . $question)}}</strong></p>
                                    <p>{{$answer}}</p>
                                @endforeach
                            </div>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
        @if(method_exists($allwhitelists, 'links'))
            <div class="container">
                <div class="pagination justify-content-center p-4">
                    {{$allwhitelists->links()}}
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
