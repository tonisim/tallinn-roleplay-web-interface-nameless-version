@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="container">
                @component('components.searchBar', ['routeName' => 'adminSearchUser', 'placeholder' => 'Otsi isikut'])
                @endcomponent
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{__('headers.user_data')}}</div>

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (isset($isCkCandidate) && $isCkCandidate)
                            <div class="alert alert-danger" role="alert">
                                {{strtoupper('user have jail time more then 300 minutes')}}
                            </div>
                        @endif
                        {{-- ADMIN ACTIONS--}}
                        <div class="card-header"><b>{{__('headers.actions')}}</b></div>
                        <div class="card-body">
                            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                <form method="POST" action="{{route('adminCharacterKill')}}" class="d-inline-block"
                                      onsubmit="return ConfirmDelete()">
                                    @csrf
                                    <input type="hidden" name="identifier" value="{{$user->identifier}}">
                                    <button type="submit" class="btn btn-danger" onclick="overlayOn()">
                                        {{__('buttons.delete_user')}}
                                    </button>
                                </form>
                            @endif

                            @if($isWhitelisted)
                                @if($isWhitelistBanned)
                                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                        <form method="POST" action="{{route('adminBanFromWhitelist')}}"
                                              class="d-inline-block">
                                            @csrf
                                            <input type="hidden" name="identifier" value="{{$user->identifier}}">
                                            <button type="submit" class="btn btn-warning" onclick="overlayOn()">
                                                {{__('buttons.un_ban_user_from_whitelist')}}
                                            </button>
                                        </form>
                                    @endif
                                @else
                                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                        <button type="submit" class="btn btn-warning" data-toggle="collapse"
                                                href="#banForm"
                                                aria-expanded="false"
                                                aria-controls="banForm">
                                            {{__('buttons.ban_user_from_whitelist')}}
                                        </button>
                                    @endif
                                    <button type="submit" class="btn btn-dark" data-toggle="collapse"
                                            href="#warningForm"
                                            aria-expanded="false"
                                            aria-controls="warningForm">
                                        {{__('buttons.user_warnings')}}
                                    </button>
                                    <form method="POST" action="{{route('adminBanFromWhitelist')}}"
                                          id="banForm"
                                          class="collapse mt-3 mb-3" onsubmit="overlayOn()">
                                        @csrf
                                        <input type="hidden" name="identifier" value="{{$user->identifier}}">
                                        <label for="banReason">{{ __('forms.reason') }}:</label>
                                        <textarea name="banReason" class="form-control"
                                                  placeholder="{{ __('forms.reason') }}" required></textarea>
                                        <button type="submit" class="btn btn-success mt-3" onclick="overlayOn()">
                                            {{__('buttons.confirm')}}
                                        </button>
                                    </form>
                                @endif
                            @endif


                            <form method="POST" action="{{route('userWarning')}}"
                                  id="warningForm"
                                  class="collapse mt-3 mb-3" onsubmit="overlayOn()">
                                @csrf
                                <input type="hidden" name="identifier" value="{{$user->identifier}}">
                                <label for="banReason">{{ __('forms.reason') }}:</label>
                                <textarea name="banReason" class="form-control"
                                          placeholder="{{ __('forms.reason') }}" required></textarea>
                                <button type="submit" class="btn btn-success mt-3" onclick="overlayOn()">
                                    {{__('buttons.confirm')}}
                                </button>
                            </form>

                            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                @component('admin.components.userWhitelistBans',['banReasons' => $banReasons])
                                @endcomponent
                            @endif
                                @component('admin.components.userWarnings', ['warningReasons' => $warningReasons])
                                @endcomponent
                            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                @component('admin.components.adminUserData', ['userData' => $user])
                                @endcomponent
                                @component('admin.components.adminUserInventory', ['userinventory' => $userinventory])
                                @endcomponent
                                @component('admin.components.adminBills', ['bills' => $bills,  'identifier' => $user->identifier])
                                @endcomponent
                                @component('components.licenses', ['licenses' => $licenses, 'identifier' => $user->identifier])
                                @endcomponent
                                @component('police.components.userCars', ['cars' => $cars,  'owner' => $user, 'collapse' => true])
                                @endcomponent
                                @component(
                                    'admin.components.adminCriminalRecords',
                                    [
                                        'characters' => $characters,
                                        'criminalRecords' => $criminalRecords,
                                        'actions' => false,
                                        'criminalIdentifier' => $user->identifier
                                    ]
                                )
                                @endcomponent
                                @component('admin.components.adminMedical', ['characters' => $characters, 'medicalHistory' => $medicalHistory, 'actions' => false, 'identifier' => $user->identifier])
                                @endcomponent
                                @component('components.characters', ['characters' => $characters, 'actions' => false, 'police' => false, 'identifier' => $user->identifier])
                                @endcomponent
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        <script>

            /**
             *
             * @returns {boolean}
             * @constructor
             */
            function ConfirmDelete() {
                var x = confirm("Are you sure you want to continue?");
                if (x)
                    return true;
                else
                    return false;
            }

        </script>
