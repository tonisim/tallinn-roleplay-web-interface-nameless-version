<div class="card-header mouse-over" data-toggle="collapse"
     href="#userBills"
     aria-expanded="false"
     aria-controls="userBills">
    <b>{{__('headers.bills')}}</b>
    <span class="badge badge-pill badge-secondary">
        {{\App\Bill::where('identifier', $identifier)->get()->count()}}
    </span>
</div>

<div class="mb-3  collapse" id="userBills">
    <div class="card-body">
        <div class="card-body">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.label')}} </th>
                    <th scope="col">{{__('tables.issuer')}}</th>
                    <th scope="col">{{__('tables.receive')}}</th>
                    <th scope="col">{{__('tables.amount')}}</th>
                    <th scope="col">{{__('tables.actions')}}</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($bills as $bill)
                    <tr>
                        <td scope="row">{{$bill->label}}</td>
                        <td>{{\App\User::find($bill->sender)->name}}</td>
                        <td>{{$bill->target}}</td>
                        <td>{{$bill->amount}}</td>
                        <td>
                            <form method="POST" action="{{route('payUserBill')}}">
                                @csrf
                                <input type="hidden" name="billTarget" value="{{$bill->target}}">
                                <input type="hidden" name="billId" value="{{$bill->id}}">
                                <input type="hidden" name="billOwner" value="{{$bill->identifier}}">
                                <button type="submit" class="btn btn-primary"
                                        onclick="overlayOn()">{{__('buttons.forcePay')}}</button>
                            </form>
                        </td>
                        <td>
                            <form method="POST" action="{{route('policeDeleteBill')}}">
                                @csrf
                                <input type="hidden" name="billId" value="{{$bill->id}}">
                                <input type="hidden" name="billOwner" value="{{$bill->identifier}}">
                                <button type="submit" class="btn btn-danger" onclick="overlayOn()">{{__('buttons.delete')}}</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

