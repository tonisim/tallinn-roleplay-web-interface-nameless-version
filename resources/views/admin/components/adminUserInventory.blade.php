<div class="card-header mouse-over" data-toggle="collapse"
     href="#userInventory"
     aria-expanded="false"
     aria-controls="userData">
    <b>{{__('headers.user_inventory')}}</b>

</div>


<div class="mb-3 collapse" id="userInventory">
    <div class="card-body">
        <div class="card-body">
            <div class="row ">
                @foreach($userinventory  as $inventory)
                    @if($inventory->count > 0)
                        <div class="col-md-3">
                            <p><strong>{{$inventory->label}} |</strong> {{$inventory->count}}</p>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>

