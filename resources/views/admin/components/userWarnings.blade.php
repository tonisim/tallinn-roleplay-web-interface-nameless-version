<div class="card-header mouse-over" data-toggle="collapse"
     href="#userWarnings"
     aria-expanded="false"
     aria-controls="userWarnings">
    <b>{{__('headers.warnings')}}</b>
    <span class="badge badge-pill badge-danger">
        {{$warningReasons->count()}}
    </span>
</div>


<div class="mb-3  collapse" id="userWarnings">
    <div class="card-body">
        @foreach($warningReasons  as $warningReason)
            <div class="border-bottom mt-3">
                <p><strong>{{__('texts.warning')}}:</strong> {{$warningReason->reason}}</p>
                <p><strong>{{__('texts.date')}}:</strong> {{$warningReason->created_at}}</p>
                <p><strong>{{__('texts.addedby')}}:</strong> {{$warningReason->adminname}}</p>
            </div>
        @endforeach
    </div>
</div>

