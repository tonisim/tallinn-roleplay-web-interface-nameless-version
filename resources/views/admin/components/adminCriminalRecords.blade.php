<div class="card-header mouse-over" data-toggle="collapse"
     href="#criminalRecordsBody"
     aria-expanded="false"
     aria-controls="criminalRecordsBody">
    <b>{{__('headers.criminal_records')}}</b>
    <span class="badge badge-pill badge-secondary">{{$criminalRecords->count()}}</span>
    <span class="badge badge-pill badge-danger">
        {{\App\CriminalRecord::where('user_identifier', $criminalIdentifier)
        ->where('is_criminal_case', true)
            ->where('criminal_case_expires_at', '>', new DateTime())
            ->get()
            ->count()
        }}
    </span>
</div>

@foreach($characters as $character)
    <div class="mb-3  collapse" id="criminalRecordsBody">
        <div class="card-body">
            <div class="border-bottom">
                <h4>{{__('texts.warnings')}}:</h4>
                <p>
                    <strong>{{__('texts.traffic_violations')}}: </strong>
                    {{$criminalRecords->where('warning_driver_license', true)->count()}}
                </p>
                <p>
                    <strong>{{__('texts.warnings_about_car')}}: </strong>
                    {{$criminalRecords->where('warning_car', true)->count()}}
                </p>
                <p>
                    <strong>{{__('texts.gun_violations')}}: </strong>
                    {{$criminalRecords->where('warning_gun_license', true)->count()}}
                </p>
            </div>
            <div class="mt-4">
                @if(isset($createRecordButton))
                    {{ $createRecordButton }}
                @endif
            </div>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th scope="col">{{__('tables.jail_time')}}</th>
                    <th scope="col">{{__('tables.summary')}}</th>
                    <th scope="col">{{__('tables.issuer')}}</th>
                    <th scope="col">{{__('tables.is_criminal_case')}}</th>
                    <th scope="col">{{__('tables.created_at')}}</th>
                    <th scope="col">{{__('tables.criminal_case_expires_at')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($criminalRecords as $criminalRecord)
                    @if($character->firstname . ' ' . $character->lastname === $criminalRecord->character_name)
                        @if($criminalRecord->is_criminal_case and new DateTime($criminalRecord->criminal_case_expires_at) > new DateTime())
                            <tr class="alert-danger mouse-over"
                                onclick="
                                    window.location='{{route('policeShowReport', ['id' => $criminalRecord->id])}}';
                                    overlayOn();
                                    "
                            >
                        @else
                            <tr
                                class="mouse-over"
                                onclick="
                                    window.location='{{route('policeShowReport', ['id' => $criminalRecord->id])}}';
                                    overlayOn();
                                    "
                            >
                                @endif
                                <td>
                                    {{$criminalRecord->character_name}}
                                </td>
                                <td>
                                    {{$criminalRecord->jail_time}}
                                </td>
                                <td>
                                    @if(strlen($criminalRecord->summary) > 250)
                                        {{substr($criminalRecord->summary, 0, 250)}}
                                    @else
                                        {{$criminalRecord->summary}}
                                    @endif
                                </td>
                                <td>
                                    @if($criminalRecord->policeOfficer()->first()->user()->first())
                                        {{\App\Helpers\UserHelper::getCharacterName($criminalRecord->policeOfficer()->first()->user()->first())}}
                                    @endif
                                </td>
                                <td>
                                    {{($criminalRecord->is_criminal_case) ? 'Yes' : 'No'}}
                                </td>
                                <td>
                                    {{$criminalRecord->created_at}}
                                </td>
                                <td>
                                    {{$criminalRecord->criminal_case_expires_at}}
                                </td>
                            </tr>
                        @endif
                        @endforeach
                </tbody>
            </table>
        </div>
        @endforeach
    </div>
