@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">

            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.record_id')}}</th>
                    <th scope="col">{{__('tables.character_name')}}</th>
                    <th scope="col">{{__('tables.deleter')}}</th>
                    <th scope="col">{{__('tables.reason')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allDeletedRecords as $deletedRecord)

                    <tr>
                        <th scope="row" data-toggle="collapse" data-target="#accordion{{$deletedRecord->id}}" class="clickable">
                            {{ $deletedRecord->record }}
                        </th>
                        <th scope="row" data-toggle="collapse" data-target="#accordion{{$deletedRecord->id}}" class="clickable">
                            {{ $deletedRecord->characterName }}
                        </th>
                        <th scope="row" data-toggle="collapse" data-target="#accordion{{$deletedRecord->id}}" class="clickable">
                            {{ $deletedRecord->deleter }}
                        </th>
                        <th scope="row" data-toggle="collapse" data-target="#accordion{{$deletedRecord->id}}" class="clickable">
                            {{ $deletedRecord->reason }}
                        </th>
                    </tr>

                    <tr>
                        <td colspan="12">
                            <div id="accordion{{$deletedRecord->id}}" class="collapse">
                                <p><b>Kokkuvõte:</b> {{$deletedRecord->summary}}</p>
                                <hr>
                                <p><b>Trahvid:</b>
                                @if(isset($deletedRecord->fine))
                                    @foreach( json_decode($deletedRecord->fine) as $fine )
                                        <p>{{ $fine->label }} {{ $fine->amount }}€</p>
                                    @endforeach
                                @endif
                            </div>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
            @if(method_exists($allDeletedRecords, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$allDeletedRecords->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
