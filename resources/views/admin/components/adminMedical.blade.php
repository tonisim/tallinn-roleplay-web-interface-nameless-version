<div class="card-header mouse-over" data-toggle="collapse"
     href="#userMedical"
     aria-expanded="false"
     aria-controls="userMedical">
    <b>{{__('headers.medical_history')}}</b>
    <span class="badge badge-pill badge-danger">
        {{$medicalHistory->count()}}
    </span>
</div>


<div class="mb-3  collapse" id="userMedical">
    <div class="card-body">
        @foreach($characters as $character)
            <div class="mb-3">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">{{__('tables.character_name')}}</th>
                        <th>{{__('tables.summary')}}</th>
                        <th>{{__('tables.issuer')}}</th>
                        <th>{{__('tables.created_at')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($medicalHistory as $medicalIncident)
                        @if($character->firstname . ' ' . $character->lastname === $medicalIncident->characterName)
                            <tr class="mouse-over"
                                onclick="
                                    window.location='{{route('showMedicalIncident', ['id' => $medicalIncident->id])}}';
                                    overlayOn();
                                    ">
                                <td>
                                    {{$medicalIncident->characterName}}
                                </td>
                                <td>
                                    @if(strlen($medicalIncident->summary) > 20)
                                        {{substr($medicalIncident->summary, 0, 20)}}
                                    @else
                                        {{$medicalIncident->summary}}
                                    @endif
                                </td>
                                <td>
                                    @if ($medicalIncident->emsWorker()->first()->user()->first())
                                        {{\App\Helpers\UserHelper::getCharacterName($medicalIncident->emsWorker()->first()->user()->first())}}
                                    @endif
                                </td>
                                <td>
                                    {{$medicalIncident->created_at}}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
</div>
