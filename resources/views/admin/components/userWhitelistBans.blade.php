<div class="card-header mouse-over" data-toggle="collapse"
     href="#wlbans"
     aria-expanded="false"
     aria-controls="wlbans">
    <b>{{__('headers.ban_reasons')}}</b>
    <span class="badge badge-pill badge-danger">
        {{$banReasons->count()}}
    </span>
</div>


<div class="mb-3  collapse" id="wlbans">
    <div class="card-body">
        @foreach($banReasons as $banReason)
            <div class="border-bottom mt-3">
                <p><strong>{{__('texts.ban_reason')}}:</strong> {{$banReason->reason}}</p>
                <p><strong>{{__('texts.date')}}:</strong> {{$banReason->created_at}}</p>
            </div>
        @endforeach
    </div>
</div>
