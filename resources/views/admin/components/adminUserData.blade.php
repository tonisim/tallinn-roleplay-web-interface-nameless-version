<div class="card-header mouse-over" data-toggle="collapse"
     href="#userData"
     aria-expanded="false"
     aria-controls="userData">
    <b>{{__('headers.user_data')}}</b>

</div>


<div class="mb-3  collapse" id="userData">
    <div class="card-body">
        <div class="card-body">
            <p><strong>{{__('texts.steam_id')}}</strong> {{ $userData->identifier }}</p>
            <p><strong>{{__('texts.five_m_steam_user')}}: </strong>{{ $userData->name }}</p>
            <p><strong>{{__('texts.character_name')}}: </strong>{{ $userData->firstname }} {{$userData->lastname}}</p>
            <p><strong>{{__('texts.date_of_birth')}}: </strong>{{ $userData->dateofbirth }}</p>
            <p><strong>{{__('texts.sex')}}: </strong>{{ $userData->sex }}</p>
            <p><strong>{{__('texts.height')}}: </strong>{{ $userData->height }}</p>
            <p><strong>{{__('texts.cash')}}: </strong>{{ $userData->money }}</p>
            <p><strong>{{__('texts.money_on_account')}}: </strong>{{ $userData->bank }}</p>
            <p><strong>{{__('texts.job')}}: </strong>
                {{ \App\Job::where('name', $userData->job)->first()->label }}
                {{ \App\JobGrades::where('job_name', $userData->job)->where('grade', $userData->job_grade)->first()->label }}
            </p>


        </div>
    </div>
</div>

