@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">{{__('tables.confiscator')}}</th>
                    <th>{{__('tables.number_plate')}}</th>
                    <th>{{__('tables.carOwnerName')}}</th>
                    <th>{{__('tables.confiscatedTime')}}</th>
                    <th>{{__('tables.actions')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cars as $car)
                    <tr>
                        <td>{{$car->confiscatorName}}</td>
                        <td>{{$car->carPlate}}</td>
                        <td>{{$car->carOwnerName}}</td>
                        <td>{{$car->created_at}}</td>
                        <td>

                            <form method="POST" action="{{route('policeReturnCar')}}"
                                  class="d-inline-block"
                                  onsubmit="return ConfirmDelete(); overlayOn();">
                                @csrf
                                <input type="hidden" name="returnCarPlate" value="{{$car->carPlate}}">
                                <input type="hidden" name="returnCarOwner" value="{{$car->carOwner}}">
                                <button type="submit" class="btn btn-danger">
                                    {{__('buttons.return_car')}}</button>
                            </form>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>

            @if(method_exists($cars, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$cars->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
