@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-gray">
            {{__('headers.fill_criminal_record_for')}}
            <a href="{{route('policeSingleUser', ['steamId' => $criminalIdentifier])}}">
                {{\App\Helpers\UserHelper::getCharacterName(\App\User::find($criminalIdentifier))}}
            </a>
        </h2>
        @if ($errors->any())
            <div class="alert alert-danger mt-4">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card mt-4">
            <div class="card-body">
                <form method="POST"
                      @if(isset($criminalRecord))
                      action="{{route('updateCriminalRecord')}}"
                      @else
                      action="{{route('addCriminalRecord')}}"
                    @endif
                >
                    @csrf
                    <input type="hidden" name="officerId" value="{{$officerIdentifier}}">
                    <input type="hidden" name="criminalId" value="{{$criminalIdentifier}}">
                    @isset($criminalRecord)
                        <input type="hidden" name="recordId" value="{{$criminalRecord->id}}">
                    @endisset
                    <div class="form-row">
                        <div class="form-group col-md-6 mt-4">



                            @if(isset($criminalRecord))
                                <p><b>{{__('texts.chosen_fines')}}</b></p>
                                @foreach($fines as $fine)
                                    <p>{{$fine->label}} {{$fine->amount}}</p>
                                @endforeach
                            @else
                                <p><b>{{__('texts.choose_fines')}}</b></p>
                                <select class="js-select col-md-12" id="punishment" onchange="selectFunction(event)" title="Vali Trahv"  multiple data-live-search="true" name="finesToPay[]">
                                    @foreach($fines as $fine)
                                        <option value="{{$fine->id}}" data-amount="{{$fine->amount}}" data-min-jail="{{$fine->min_jail}}" data-max-jail="{{$fine->max_jail}}" data-min-com="{{$fine->com_service_min}}" data-max-com="{{$fine->com_service_max}}" >{{$fine->label}} | {{$fine->amount}}</option>

                                    @endforeach

                                </select>
                            @endif



                        </div>
                        @if(isset($criminalRecord))

                        @else
                            <div class="form-group col-md-3 mt-4">
                                <p><b>{{__('texts.jail_time')}}</b></p>
                                <p>Minimaalne: <span class="minJail">0</span></p>
                                <p>Maksimaalne: <span class="maxJail">0</span></p>

                            </div>
                            <div class="form-group col-md-3 mt-4">
                                <p><b>{{__('texts.com_time')}}</b></p>
                                <p>Minimaalne: <span class="minCom">0</span></p>
                                <p>Maksimaalne: <span class="maxCom">0</span></p>
                            </div>

                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6" style="padding:0;">
                            <hr style="border-top: 1px solid rgba(0, 100, 97, 0.5)">
                            <p>Trahvi summa: <span class="sumAmount">0</span></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="jailTime">{{__('forms.jail_time')}}</label>
                            <input id="jailTime" name="jailTime" type="number" class="form-control"
                                   @if(isset($criminalRecord))
                                   value="{{$criminalRecord->jail_time}}"
                                   @else
                                   value="0"
                                @endif
                            >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="place">{{__('forms.place')}}</label>
                            <input id="place" name="place" type="text" class="form-control"
                                   @if(isset($criminalRecord->place))
                                   value="{{$criminalRecord->place}}"
                                @endif
                            >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="carPlate">{{__('forms.car_plate')}}</label>
                            <input id="carPlate" name="carPlate" type="text" class="form-control"
                                   @if(isset($criminalRecord->car_plate))
                                   value="{{$criminalRecord->car_plate}}"
                                @endif
                            >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <p class="font-weight-bold">{{__('texts.warnings')}}</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="car" name="warningCar"
                                       @if(isset($criminalRecord) and $criminalRecord->warning_car)
                                       checked
                                    @endif
                                >
                                <label class="form-check-label" for="car">
                                    {{__('forms.make_warning_about_car')}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="driverLicense"
                                       name="warningDriverLicense"
                                       @if(isset($criminalRecord) and $criminalRecord->warning_driver_license)
                                       checked
                                    @endif
                                >
                                <label class="form-check-label" for="driverLicence">
                                    {{__('forms.make_warning_about_traffic_violation_drives_license')}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="gunLicense" name="warningGunLicense"
                                       @if(isset($criminalRecord) and $criminalRecord->warning_gun_license)
                                       checked
                                    @endif
                                >
                                <label class="form-check-label" for="gunLicense">
                                    {{__('forms.make_warning_about_gun_law_violation_gun_licence')}}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <p class="font-weight-bold">{{__('texts.is_this_criminal_case')}}</p>
                            <input type="radio" id="criminalCaseTrue" name="criminalCase" value="{{true}}"
                                   @if(isset($criminalRecord) and $criminalRecord->is_criminal_case)
                                   checked
                                @endif
                            >
                            <label for="criminalCaseTrue">{{__('forms.it_s_criminal_case')}}</label><br>
                            <input type="radio" id="criminalCaseFalse" name="criminalCase" value="{{false}}"
                                   @if(isset($criminalRecord) and !$criminalRecord->is_criminal_case)
                                   checked
                                @endif
                            >
                            <label for="criminalCaseFalse">{{__('forms.it_s_not_criminal_case')}}</label><br>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="otherParties">{{__('forms.other_parties')}}</label>
                            <textarea class="form-control" id="otherParties" name="otherParties" rows="2"
                            >@isset($criminalRecord){{$criminalRecord->other_parties}}@endisset</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="summary">{{__('forms.summary')}}</label>
                            <textarea class="form-control" id="summary" name="summary" rows="4"
                                      required>@isset($criminalRecord){{trim($criminalRecord->summary, "\x00..\x1F")}}@endisset</textarea>
                        </div>
                    </div>
                    @if(isset($criminalRecord))
                        <button class="btn btn-info" id="confirmButton" onclick="overlayOn()"
                                disabled>{{__('buttons.edit_record')}}</button>
                    @else
                        <button class="btn btn-info" id="confirmButton" onclick="overlayOn()"
                                disabled>{{__('buttons.create_record')}}</button>
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
<script>


    window.onload = function () {
        const summaryField = document.getElementById('summary');
        const jailTimeField = document.getElementById('jailTime');
        const confirmButton = document.getElementById('confirmButton');
        let elementArray = [
            summaryField,
            jailTimeField
        ];

        elementArray.forEach(function (element) {
            element.addEventListener('keyup', function (event) {
                let isValidSummary = summaryField.checkValidity();
                let isValidJailTimeField = jailTimeField.checkValidity();

                if (isValidSummary && isValidJailTimeField) {
                    confirmButton.disabled = false;
                } else {
                    confirmButton.disabled = true;
                }
            });
        });
    }
</script>
