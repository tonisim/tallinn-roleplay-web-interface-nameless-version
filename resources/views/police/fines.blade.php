@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(isset($success))
                    <div class="alert alert-success">
                        {{$success}}
                    </div>
                @endif
                @component(
                    'components.searchBarWithoutAutocomplite',
                    [
                        'routeName' => $fineSearchRoute,
                        'placeholder' => __('forms.search_fine'),
                        'oldSearch' => (isset($oldSearch)) ? $oldSearch : null
                    ]
                )
                @endcomponent
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><b>{{__('headers.fines')}}</b></div>
                    <div class="card-body">
                        @if (!Auth::guest() and (
                            \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                            (isset(\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job)
                                and
                                in_array(
                                    \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                     \App\Http\Controllers\PoliceController::ALLOWED_JOBS
                                 )
                                 and
                                 in_array(
                                     \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job_grade,
                                      \App\Http\Controllers\PoliceController::ALLOWED_JOB_GRADES
                                  )
                             )
                        ))
                            <a class="btn btn-info mb-4" href="{{route('showAddNewFine')}}"
                               onclick="overlayOn()">{{__('buttons.add_new_fines')}}</a>
                       @endif
                    </div>

                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">{{__('tables.description')}}</th>
                            <th scope="col">{{__('tables.amount')}}</th>
                            <th scope="col">{{__('tables.min_jail')}}</th>
                            <th scope="col">{{__('tables.max_jail')}}</th>
                            <th scope="col">{{__('tables.com_service_min')}}</th>
                            <th scope="col">{{__('tables.com_service_max')}}</th>
                            @if (!Auth::guest() and (
                                \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                                (isset(\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job)
                                    and
                                    in_array(
                                        \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                         \App\Http\Controllers\PoliceController::ALLOWED_JOBS
                                     )
                                     and
                                     in_array(
                                         \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job_grade,
                                          \App\Http\Controllers\PoliceController::ALLOWED_JOB_GRADES
                                      )
                                 )
                            ))
                                <th scope="col">{{__('tables.category')}}</th>
                                <th scope="col">{{__('tables.actions')}}</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fines as $fine)
                            <tr>
                                <td>
                                    {{$fine->label}}
                                </td>
                                <td>
                                    {{$fine->amount}}
                                </td>
                                <td>
                                    {{$fine->min_jail}}
                                </td>
                                <td>
                                    {{$fine->max_jail}}
                                </td>
                                <td>
                                    {{$fine->com_service_min}}
                                </td>
                                <td>
                                    {{$fine->com_service_max}}
                                </td>
                                @if (!Auth::guest() and (
                                    \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                                    (isset(\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job)
                                        and
                                        in_array(
                                            \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                             \App\Http\Controllers\PoliceController::ALLOWED_JOBS
                                         )
                                         and
                                         in_array(
                                             \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job_grade,
                                              \App\Http\Controllers\PoliceController::ALLOWED_JOB_GRADES
                                          )
                                     )
                                ))
                                    <td>
                                        {{$fine->category}}
                                    </td>
                                    <td>
                                        <a class="btn-info btn m-1" href="{{route('showEditFine', ['id' => $fine->id])}}"
                                           onclick="overlayOn()">
                                            {{__('buttons.edit')}}
                                        </a>
                                        <form action="{{route('removeFine')}}" method="post" class="d-inline-block">
                                            @csrf
                                            <input type="hidden" value="{{$fine->id}}" name="id">
                                            <button class="btn btn-danger m-1" type="submit" onclick="overlayOn()">
                                                {{__('buttons.remove')}}
                                            </button>
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(method_exists($fines, 'links'))
                        <div class="container">
                            <div class="pagination justify-content-center p-4">
                                {{$fines->links()}}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
