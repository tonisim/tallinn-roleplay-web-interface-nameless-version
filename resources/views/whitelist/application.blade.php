@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header justify-content-center">
                        {{__('headers.whitelist_application_with_web_user_registration')}}
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('whitelistApplicationStore') }}">
                            @csrf

                            <div class="form-group row justify-content-center">
                                <b>{{__('texts.web_user_data')}}:</b>
                            </div>
                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" required autofocus placeholder="{{ __('forms.name') }}">

                                    <span class="small">{{__('texts.name_that_is_showed_in_web_when_you_are_logged_in')}}</span>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required placeholder="{{__('forms.e_mail_address')}}">
                                    <span class="small">{{__('texts.used_for_logging_into_web_page')}}</span>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required placeholder="{{__('forms.password')}}">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">

                                <div class="col-md-12">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required placeholder="{{__('forms.confirm_password')}}">
                                </div>
                            </div>

                            @component('components.whitelistApplication')
                            @endcomponent

                            <div class="form-group row mb-0">
                                <div class="col-md-1">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('buttons.register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
