@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row align-items-center d-flex text-center mt-5 justify-content-center namelessdisc">
                <div id="discordInviteBox"></div>
        </div>
    </div>

    @component('components.spinner')
    @endcomponent
@endsection
