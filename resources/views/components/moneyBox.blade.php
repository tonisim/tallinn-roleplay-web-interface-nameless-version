@component('components.box')
    <div class="row">
    <div class="col-md-6 text-center mt-4 mb-4">
        <i class="far fa-money-bill-alt fa-3x text-success text-center"></i>
        <div class="mt-0 align-middle font-weight-bold" style="font-size: 20px;">{{number_format($data->money, 0 ,'.', ' ')}}</div>
    </div>
    <div class="col-md-6 text-center mt-4 mb-4">
        <i class="far fa-credit-card fa-3x text-success text-center"></i>
        <div class="ml-3 align-middle font-weight-bold" style="font-size: 20px;">{{number_format($data->bank, 0, '.', ' ')}}</div>
    </div>
    </div>
@endcomponent
