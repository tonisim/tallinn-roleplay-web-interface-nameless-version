@component('components.box')
    <div class="row">

        <div class="col-md-12 text-center mt-4">
            <p>Nimi: {{\App\Helpers\UserHelper::getCharacterName($user)}} ({{$user->sex}})</p>
            <p>Sünniaeg: {{$user->dateofbirth}}</p>
            <p>Pikkus: {{$user->height}} cm</p>
        </div>
    </div>
@endcomponent
