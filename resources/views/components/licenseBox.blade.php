@component('components.box')
    <div class="row">
        <div class="col-md-12 text-center mt-5 mb-4 licensebox">
            <i class="fa fa-id-card-o fa-3x text-success text-center" aria-hidden="true"></i>
                @foreach($licenses as $license)
                    @switch($license->type)
                        @case('dmv')
                        <p>{{__('texts.theory_exam')}}</p>
                        @break
                        @case('drive')
                        <p>{{__('texts.car_driver_license')}}</p>
                        @break
                        @case('drive_bike')
                        <p>{{__('texts.bike_driver_license')}}</p>
                        @break
                        @case('drive_truck')
                        <p>{{__('texts.truck_driver_license')}}</p>
                        @break
                        @case('drive_bus')
                        <p>{{__('texts.bus_driver_license')}}</p>
                        @break
                        @case('boat')
                        <p>{{__('texts.boat_driver_license')}}</p>
                        @break
                        @case('weapon')
                        <p>{{__('texts.concealed_weapon_carry_license')}}</p>
                        @break
                        @case('fishing')
                        <p>{{__('texts.fishing_license')}}</p>
                        @break
                        @case('hunting')
                        <p>{{__('texts.hunting_license')}}</p>
                        @break
                    @endswitch
                @endforeach
        </div>
    </div>
@endcomponent
