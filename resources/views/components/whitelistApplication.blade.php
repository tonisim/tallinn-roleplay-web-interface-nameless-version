<div class="form-group row justify-content-center mt-5">
    <b>{{__('texts.five_m_server_user_and_character_data')}}:</b>
</div>
@php
    if(isset($whitelistData) and $whitelistData->extra_questions != "null" and $whitelistData->extra_questions != null) {
       $extraQuestions = json_decode($whitelistData->extra_questions);
    }
@endphp
<div class="form-group row">
    <div class="col-md-12">
        <input id="characterName" type="text"
               class="form-control"
               name="characterName"
               value="{{ isset($whitelistData) ? $whitelistData->characterName : old('characterName') }}" required
               placeholder="{{__('forms.character_name')}}"
        >
        <span class="small">{{__('texts.character_name_that_you_are_planing_to_use_in_game')}}</span>
        @if ($errors->has('characterName'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('characterName') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <input id="steamName" type="text"
               class="form-control"
               name="steamName" value="{{ isset($whitelistData) ? $whitelistData->steamName : old('steamName') }}"
               required placeholder="{{__('forms.steam_name')}}">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <input id="hex" type="text"
               class="form-control"
               name="hex" value="{{isset($whitelistData) ? substr($whitelistData->identifier, 6) :  old('hex') }}"
               required placeholder="{{__('forms.steam_hex')}}">
        <span class="small">{{__('texts.hex_can_be_found_by_visiting')}}
            <a target="_blank" href="http://vacbanned.com/">http://vacbanned.com/</a>
        </span>
        @if ($errors->has('hex'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('hex') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row justify-content-center mt-5">
    <h5>{{__('texts.extra_questions')}}:</h5>
</div>
<div class="form-group row justify-content-center">
    <p>
        <strong>
            {{__('texts.read_rules')}}<a href="{{route('help')}}" target="_blank">{{__('texts.rules_link')}}</a>
        </strong>
    </p>
</div>
<div class="form-group row">
    <div class=" col-md-12">
        <label for="age">{{__('forms.age')}}</label>
        <input id="age" type="number"
                  class="form-control"
                  name="age" required
        >{{isset($extraQuestions->age) ? $extraQuestions->age :  old('age') }}
        @if ($errors->has('age'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('age') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
        <label for="discord">{{__('forms.discord')}}</label>
        <textarea id="discord" type="text"
                  class="form-control"
                  name="discord" required
        >{{isset($extraQuestions->discord) ? $extraQuestions->discord :  old('discord') }}</textarea>
        @if ($errors->has('discord'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('discord') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
        <label for="biography_description">{{__('forms.biography_description')}}</label>
        <textarea id="biography_description" type="text"
                  class="form-control"
                  name="biography_description" required
        >{{isset($extraQuestions->biography_description) ? $extraQuestions->biography_description :  old('biography_description') }}</textarea>
        @if ($errors->has('biography_description'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('biography_description') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
        <label for="rprules">{{__('forms.rprules')}}</label>
        <textarea id="rprules" type="text"
                  class="form-control"
                  name="rprules" required
        >{{isset($extraQuestions->rprules) ? $extraQuestions->rprules :  old('rprules') }}</textarea>
        @if ($errors->has('rprules'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('rprules') }}</strong>
            </p>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class=" col-md-12">
        <label for="situation">{{__('forms.situation')}}</label>
        <textarea id="situation" type="text"
                  class="form-control"
                  name="situation" required
        >{{isset($extraQuestions->situation) ? $extraQuestions->situation :  old('situation') }}</textarea>
        @if ($errors->has('situation'))
            <p>
                <strong style="color: red; font-size: 14px">{{ $errors->first('situation') }}</strong>
            </p>
        @endif
    </div>
</div>

