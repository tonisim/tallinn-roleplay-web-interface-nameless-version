@extends('help.layout.layout')
@section('helpHeader')
    <strong>Üldine info</strong>
@endsection
@section('helpBody')
    <div>
        <h4><strong>Key binds / Nupud</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li><strong>F1</strong> - Telefon</li>
                    <li><strong>F3</strong> - Animatsioonide menüü </li>
                    <li><strong>F6</strong> - Whitelist tööde menüü</li>
                    <li><strong>F7</strong> - Arved</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>K</strong> - Inventor</li>
                    <li><strong>B</strong> - Pointing</li>
                    <li><strong>L</strong> - Masina menüü (mootor,aknad, uksed jne)</li>
                    <li><strong>Z</strong> - Kükitamine</li>
                    <li><strong>X</strong> - Käed üles</li>
                    <li><strong>Y</strong> - Voice range</li>
                    <li><strong>G</strong> - Teise isiku inventori avamine (röövimine)</li>
                    <li><strong>HOME</strong> - Scoreboard</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>LEFT CTRL</strong> - Taktikaline laskeasend</li>
                    <li><strong>ALT + 6,7,8</strong> - Politsei väljakutsetele vastamine</li>
                    <li><strong>Y + X</strong> - Meedikute distress signaalile vastamine</li>
                    <li><strong>LEFT ALT</strong> - Rääkimine (raadio ja telefon)</li>
                    <li><strong>CAPS LOCK</strong> - Püsikiirusehoidja</li>
                    <li><strong>SHIFT (HOLD)</strong> - Drift mode sõites. (NUMLOCK 9, et maha võtta)</li>
                </ul>
            </div>
        </div>
        <h4><strong>Commandid / Käsud</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li><strong>/e help2 /e help2 /e help3</strong> - Näitab kõiki /e animatsioone</li>
                    <li><strong>/e c</strong> - Eemaldab kinni jäänud propi</li>
                    <li><strong>/emote + (emote nimetus)</strong> - animatsioonide tegevus </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>/plv</strong> - Politsei announce</li>
                    <li><strong>/dsl</strong> - Diesel reklaam</li>
                    <li><strong>/lsc</strong> - LS Custom reklaam</li>
                    <li><strong>/tx</strong> - Taksopargi reklaam</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>/mikrofon või /mikrofon1</strong> - Karater võtab weazel newzi mikrofoni</li>
                    <li><strong>/kaamera</strong> - Karakter võtab weazel newzi kaamera</li>
                    <li><strong>/suits</strong> - Karakter teeb suitsu</li>
                    <li><strong>/pihku</strong> - Karakter rahuldab ennast</li>
                    <li><strong>/tassi</strong> - Kannad teist isikut seljas</li>
                    <li><strong>/risti</strong> - Karakter paneb käed risti</li>
                    <li><strong>/remondi</strong> - 24/7 mehaanik hakkab autot parandama</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
