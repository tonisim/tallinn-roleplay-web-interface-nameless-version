@extends('help.layout.layout')
@section('helpHeader')
    <strong>Serveri Reeglid</strong>
@endsection
@section('helpBody')
    <div>
        <div>
            <h4><strong>Sisukord</strong></h4>
            <ul>
                <li><a href="#RoleplayReeglid">Roleplay Reeglid</a></li>
                <li><a href="#Üldreeglid">Üldreeglid</a></li>
                <li><a href="#Röövid">Röövid</a></li>
                <li><a href="#Greenzone">Greenzone</a></li>
                <li><a href="#Discord">Discordi Reeglid</a></li>
            </ul>
        </div>

        <h4 id="RoleplayReeglid"><strong>Roleplay Reeglid</strong></h4>
        <ol>
            <li> Common Sense ja FailRP – Töötab olukordades, mida ülejäänud reeglid ei sätesta. Administratsioonil on õigus sellistes olukordades otsustada, kuidas edasi toimida. Lühidalt – ära tee midagi, mida päriselus oleks võimatu teha. Administratsioon lähtub sellistel juhtudel üldtuntud RP reeglitest, mis ei pruugi olla hetkel siia veel kirja pandud. Väärtusta inimelu!</li>
            <li>Powergame keelatud! – ära tee asju, mis päriselus oleksid võimatud. Nt. Ära RP, et tõstad ühe käega üles auto ja tõstad selle eemale.</li>
            <li>Metagame keelatud! – ära kasuta mänguvälist infot mängusiseselt. Nt. Kui sõber ütleb Discordis, et sind tullakse kohe tapma, siis seda informatsiooni mängus kasutada ei tohi, kuna see on saadud mängust väljaspool ja sinu karakter sellest midagi ei tea.</li>
            <li>RDM (Random deathmatch) – Keelatud on teiste karakterite ilma põhjuseta tapmine. Igasugune tapmine tuleb läbi RPda.</li>
            <li>Sõidukist tulevahetus - Juhikohalt tulistamine keelatud!</li>
            <li>VDM (Vehicle deathmatch) – ära sõida autoga inimestest üle, eesmärgiga neid tappa. Samuti ära pargi oma autot inimeste peale.</li>
            <li>Uue elu reegel – pärast haiglas ärkamist ei mäleta sa, mis sinuga juhtus. Samuti on keelatud tagasi minna kohta, kus surid, kui haiglas ärkamisest on möödas vähem kui 15 minutit. Revenge kill (oma tapja tapmine kättemaksuks) on samuti keelatud.</li>
            <li>FearRP – Kui sind sihitakse relvaga, pead kuuletuma. Kui sind juba sihitakse, ära võta relva välja, ära sihi vastu.</li>
            <li>Hukkamine – teist karakterit hukates tuleb informeerida hukkamisest kõiki situatsioonis osalejaid ja ka neid, kes hukatut päästma võivad tulla. Hukatu kaotab mälu ja tohib ellu ärgata ainult haiglas (kehtib uue elu reegel).</li>
            <li>Fail Driving keelatud – ära sõida autoga seal, kus reaalselt autoga sõita ei saa.</li>
            <li>Money Flood – ära jaga raha ilma reaalse RP põhjuseta. (Algab 15,000-st)</li>
            <li>Sõjaväebaas - Ära varasta sõjeväebaasi varustust.</li>
            <li>Isiku tuvastamine - Teiste mängijate tuvastamine hääle järgi on keelatud.</li>

        </ol>

        <h4 id="Üldreeglid"><strong>Üldreeglid</strong></h4>
        <ol>
            <li>Bug Abuse – ära kasuta serveris olevaid buge endale kasu saamiseks. Anna bugidest teada vastavas discord kanalis.</li>
            <li>Tööde tegemised - Tee Töötukassa töid tööauto, mitte oma isikliku autoga.</li>
            <li>Chat - Ära vaidle ja reosta OOC chati! OOC chat jäägu pigem kiirete küsimuste küsimiseks ja informeerimiseks. Kui RP olukorras on tekkinud segadus, kasutage suhtlemiseks Discordi. Kui arvad, et teine osapool on reegleid rikkunud, palu admini abi /report commandiga. Local OOC chatti on lubatud kasutada vajadusel kiirete küsimuste küsimiseks (nt. küsid commandi või teavitad teist mängijat, et ei kuule teda), RP situatsiooni üle vaidlemine ja virisemine local OOC chatis keelatud.</li>
            <li>Kauplemine - Mängusiseste asjade müümine IRL asjade või raha eest on keelatud! 18. Suhtlus - Suhtu juhtkonna ja meeskonna vastu lugupidavalt ja viisakalt. Nad kõik tegelevad serveriga vabast ajast ja teevad kõik, et kõigil oleks serveris võrdselt hea ja tore mängida.</li>
            <li>Modid ja hackid keelatud!</li>
            <li>Voice chat - Mikrofoni olemasolu kohustuslik! Ära kasuta voicechangerit, v.a. admini loaga.</li>
            <li>Ära sekku ilma põhjuseta teiste RPsse.</li>
            <li>Rassism ja vägistamine keelatud!</li>
            <li>Tõestusmaterjal - Riigitöötajatel aksepteeritav tõestusmaterjal pardakaamera, nööbikaamera. Tsiviilkodanikul võib aksepteerida ka pardakaamerat (nt. streami olemasolul), samuti võib osades olukordades arvestada screenshotte, kui nendes sisaldub reaalselt midagi tõestav informatsioon.</li>
            <li>Breaking character keelatud! Mängusiseselt suhtleme ainult karakterisiseselt. Voice chatis OOC jutuajamine keelatud.</li>
            <li>Suuremaid relvi taskust välja tõmmata ei tohi, need peavad olema seljal või hoia käes.</li>
            <li>CK vajalikkuse ja detailide üle teeb otsuse admin (ka siis, kui tahad enda karakterile CK teha), anna sellest adminile või moderaatorile märku!</li>
            <li>Teine karakter - tegemiseks küsi luba administraatorilt!</li>
            <li>Keelatud serveris AFKida (eriti narkokohtades), kui isik ei vasta 1-2 min jooksul võid kutsuda administraatori - kui administraatorile ka ei reageeri, saad kicki.</li>
            <li>/do command - valetada ei tohi! (kehtib ka vigastuste jms puhul)</li>
            <li>Copbait keelatud!</li>
            <li>Olympic Swim keelatud - s.t ujud kauem kui 5 min ilma selleks vajaliku varustuseta.</li>
            <li>Terrorism keelatud!</li>
            <li>Sisuloojad - on keelatud reklaamida sisulooja kanalis teise inimese streami ning sisulooja ei tohi streamis näidata peidetud asukohti. Reklaamiks peab olema admini luba.</li>
            <li>Identiteedi vargus - teise isiku identiteedi vargus, kuritarvitamine keelatud (ka. Sotsiaalmeediates)</li>

        </ol>


        <h4 id="Röövid"><strong>Röövid</strong></h4>
        <ol>
            <li>Pangakontodelt röövimine keelatud!</li>
            <li>Röövida tohib isikult sularaha, illegaalset raha ja muid asju.</li>
            <li>On-duty politsei/meediku röövimine - Illegaalsete asjade röövimine lubatud. Samuti võib võtta raha ja sööki-jooki. Politseivarustuse (tazer, relvad, repair kitid jms) röövimine keelatud! Operatiivsõidukite varastamine keelatud! V.a. juhtudel kus tegu on välja roleplaytud situatsiooniga.</li>
            <li>Politsei röövimine - keelatud, kui linnas ei ole vähemalt 3 on-duty politseid.</li>
            <li>Väikesed pangad Lubatud röövida maksimum 4 inimesega + pantvangid (k.a getaway driver, sniper jms)</li>
            <li>Peapank Lubatud röövida maksimum 6 inimesega + pantvangid (k.a getaway driver, sniper jms)</li>
            <li>Poerööv Lubatud röövida maksimum 3 inimesega + pantvangid (k.a getaway driver, sniper jms)</li>
            <li>Juveelipood Lubatud röövida maksimum 4 inimesega + pantvangid (k.a getaway driver, sniper jms)</li>
            <li>Pantvang -Maksimaalselt võib olla 2 pantvangi. 1 pantvang = 1 soov läbirääkimistel. 48. Sõpra pantvangiks võtta keelatud!</li>
            <li>Põgenemine korterisse - on reeglite vastane. Relvaähvardusel ei tohi sundida isikut end korterisse kutsuma eesmärgiga korteri kapp tühjaks teha.</li>
            <li>Scam - lubatud kuni 30,000€ (puhas raha). Scammida pole lubatud auto ostul-müügil. Scam maksustamata rahaga lubatud kuni 50,000€. Scammida pole lubatud esemeid (relvad, muud seadeldised).</li>
            <li>Illegaalseid tegevusi pole lubatud teha rohkem kui 6 inimesega.</li>
            <li>Relvatehingutel peab relv käima käest kätte mitte invetoryst - invetorysse. Ehk üleandmisel peab teil relv olema käes. Sama ka teisel osapoolel, kes saab relva.</li>
        </ol>

        <h4 id="Greenzone"><strong>Greenzone</strong></h4>
        <ol>
            <li>Greenzone ehk rohelised tsoonid - alad, kus igasugune kriminaalne tegevus on keelatud. Greenzone alade hulka kuuluvad nt. haigla, politseijaoskond, avalikud garaazid, töötukassa tööde alad.</li>
            <li>Greenzone baitimine – teadlikult olukorra alustamine ja siis greenzone alasse põgenemine, eesmärgiga vältida vargust või kuritegu keelatud.</li>
            <li>Kui RP olukord on alanud väljaspool greenzone ala, on mõjuval põhjusel lubatud tappa/röövida ka greenzone alas.</li>
            <li>Helikopteri või muu õhusõidukiga maandumine greenzone alas on keelatud! (Ei kehti operatiivsõidukitele)</li>
        </ol>


        <h4 id="Discord"><strong>Discordi Reeglid</strong></h4>
        <ol>
            <li>Suhtu juhtkonna ja meeskonna vastu lugupidavalt ja viisakalt. Samuti austa teisi mängijaid, et kõigil oleks võrdselt hea ja tore mängida!</li>
            <li>Jutukas ei vaidle! Selle jaoks on #💬vaidlused  kanal.</li>
            <li>Reklaam on lubatud ainult #📺sisuloojad kanalis</li>
            <li>Kui vajad abi, on selleks #🧪report  kanal.</li>
            <li>Kõikides voice channelites on kisamine, karjumine, sõimamine ja niisama trollimine keelatud! Samuti on keelatud kasutada voicechangerit.</li>
            <li>Muusika kuulamine lubatud ainult muusika voice channelis!</li>
            <li>Ära postita ebasobilikke pilte, videoid või clippe. Pildid, videod ja clipid postita #📺clipid  ja #loadingpic   channelisse. Piltide kanalist valime jooksvalt parimad välja ja lisame serveri sisselaadimis taustaks.</li>
            <li>#📺clipid  ja #loadingpic   channelis ei jutusta, ainult pildid ja videod.</li>
            <li>Ära taggi serveri administratsiooni ja developereid "üldiste" küsimustega.</li>
            <li>Ära spämmi!</li>
        </ol>


    </div>
@endsection
