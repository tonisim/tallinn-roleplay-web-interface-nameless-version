@extends('help.layout.layout')
@section('helpHeader')
    <strong>Politsei Reeglid</strong>
@endsection
@section('helpBody')
    <div>
        <h4 id="Üldreeglid"><strong>Üldreeglid</strong></h4>
        <ol>
            <li>Allud endast kõrgemale ametikohale!</li>
            <li>Korduvate vigadega on õigus juhatajal määrata disiplinaar karistus!</li>
            <li>ERARIIETES töö tegemine + niisama on-dudy istumine keelatud!</li>
            <li>OFF-DUDY minnes paned relvad relvakappi ära, need ei kuulu sulle vaid asutusele</li>
            <li>Relva ja saatja kaotamisel annad sellest koheselt teada oma otsesele ülemusele. Kui asjad on kaotatud enda süül, on sul kohustus asjad enda kuludega tagasi osta</li>
            <li>Vilkurite põhjendamatu sisse lülitamine KEELATUD!</li>
        </ol>

        <h4 id="Rööv"><strong>Rööv</strong></h4>
        <ol>
            <li>Enne südnmuskohale minekut paned selga kuulivesti ning valmistad ette vajalikud relvad jaoskonnas</li>
            <li>Kogunemine röövile reageerimiseks toimub politsei jaoskonnas või kokkulepitud kohas leitnandi või kõrgema auastmega isiku käsul</li>
            <li>Sündmuskohale jõudes EI PARGI otse ukse ette vaid hoiad piisavat DISTANTS</li>
            <li>Politsei TULD ilma asjata ei ava! (Ainult juhul kui pätt on sinu vastu tule avanud ning olukord seda soodustab)</li>
        </ol>

        <h4 id="üldinfo"><strong>Ülinfo ja Soovitused</strong></h4>

        <ul>
            <li>Määrates isikule kriminaal karistuse, tuleb isikult eemaldada kõik relvad, ebaseaduslikud ainued ja esemed ning relvaluba</li>
            <li>
                Vigastatud ja Surnud isikud:
                <ul>
                    <li>Vigastatud isikule, kui võimalik, tuleb anda abi kas kutsudes kiirabi või toimetades ta haiglasse kus saavad artstid temaga edasi tegeleda.</li>
                    <li>
                        Kui isikul puudub pulss, kui võimalik, tuleb isikule ikkagi kutsuda kiirabi või toimetada ta haiglasse. Surnuks saab kuulutada isiku ainult Arst.
                        <p class="blockquote-footer">
                            Kui viite isiku haiglasse et "Sveta" temaga tegeleks, ei tohi sundida isikut kasutama "Svetat", kuid abistatav isik peab tegema haiglas DO commandi,
                            milles on info kas isik on elus, surnud või mõnes teises seiskukorras. näiteks: /do Operatsioon ebaõnnestus ja minu elu ei õnnestunud pääasta.
                        </p>
                    </li>
                    <li>Kui isik on kuulutatud surnuks, tuleb võimalusel isik toimetada surnu kuuri kus tehakse isiku tuvastus ja asjade läbivaatus</li>
                    <li>Aitamise tähtsuse järjekord:
                        <ol>
                            <li>Politsei/Kiirabi</li>
                            <li>Kannatuanud</li>
                            <li>Kurjategijad</li>
                        </ol>
                    </li>
                </ul>
            </li>
        </ul>
        <h5 class="ml-2"><strong>Liiklus</strong></h5>
        <ol>
            <li>
                Punased Foori tuled - Kui isik ültab ristmikku punase foori tulega ohutult, on trahvi tegemine rangelt mitte soovitatav
                <p class="blockquote-footer">
                    Kuidas eirata punast tuld ohutult? Jää ristmikel foori taga seisma (nagu see oleks stop märk) ja
                    veendu et ristmikut on ohutu ületada. Arvestada tuleb sellega isikutel (ka npc-del) kellel on
                    roheline tuli, on eesõigus. Kui satub ristmikule mitu mängijat punase tule taha seisma, tuleb
                    arvestada parema käe reegliga (sinust paremal oleval autol on eesõigus)
                </p>
            </li>
            <li>
                Juhtimisõiguse peatamine - Isikult, kes rikub pidevalt liiklusseadust, võib eemaldada juhtimisõiguse.
                Juhtimisõiguse peatamisel tuleb tühistada kõik tema juhtimisõigused (k.a. teooriaeksam)
            </li>
        </ol>

        <h5 class="ml-2"><strong>Turvakontroll ja Läbi otsimine</strong></h5>
        <ol>
            <li><strong>Turvakontroll:</strong>
                Politsei või seaduses sätestatud juhul muu korrakaitseorgan võib kontrollida isikut või tema riietust
                vaatlemise ja kompimise teel või tehnilise vahendi või sellekohase väljaõppe saanud teenistuslooma abil,
                et olla kindel, et isiku valduses ei ole esemeid või aineid, millega ta võib ohustada ennast või teisi isikuid
                <ul>
                    <li>sisenemisel avaliku võimu organi ehitisse või territooriumile</li>
                    <li>kui see on vajalik kaitstava isiku või valvatava objekti ohutuse tagamiseks</li>
                    <li>kui see on vajalik kõrgendatud ohu väljaselgitamiseks, kui isik viibib elutähtsas energia-,
                        side-, signalisatsiooni-, veevarustus- või kanalisatsioonisüsteemis, liikluskorralduse ehitises
                        või seadmes või selle vahetus läheduses</li>
                    <li>kui see on vajalik vahetu kõrgendatud ohu tõrjumiseks</li>
                    <li>kui isikult võib seaduse alusel võtta vabaduse</li>
                    <li>kui käesolevas seaduses sätestatud meetme kohaldamisega kaasneb vajadus toimetada isik
                        Politsei- ja Piirivalveametisse või muu haldusorgani asukohta</li>
                    <li>Turvakontrolli kohaldamisel on õigus kasutada vahetut sundi nii kaua, kui see on eesmärgi saavutamiseks vältimatu</li>
                </ul>
            </li>
            <li><strong>Isiku läbivaatus:</strong>
                Politsei või seaduses sätestatud juhul muu korrakaitseorgan võib läbi vaadata isiku, sealhulgas isiku keha,
                kehaõõnsused, riided, riietes oleva või kehal kantava asja, kui:
                <ul>
                    <li>
                        on alust arvata, et isik kannab endaga kaasas asja või ainet, mille võib võtta seaduse alusel
                        hoiule, hõivata või konfiskeerida
                    </li>
                    <li>see on vajalik kõrgendatud ohu väljaselgitamiseks, kui isik viibib avaliku võimu toimimiseks tähtsas ehitises või selle vahetus läheduses</li>
                    <li>see on vältimatult vajalik isikusamasuse tuvastamiseks</li>
                </ul>
                Isiku läbivaatuseks, mis eeldab meditsiinilist protseduuri, toimetatakse isik tervishoiuteenuse osutaja juurde.
                Meditsiinilist protseduuri eeldavat läbivaatust võib teha ainult tervishoiutöötaja.<br>
                Isiku läbivaatusel on õigus kasutada vahetut sundi nii kaua, kui see on eesmärgi saavutamiseks vältimatu.
            </li>
            <li><strong> Vallasasja läbivaatus:</strong>
                Politsei või seaduses sätestatud juhul muu korrakaitseorgan võib valdaja nõusolekuta kontrollida meeleliselt
                või tehnilise vahendi või teenistuslooma abil vallasasja, sealhulgas avada uksi ja kõrvaldada muid takistusi, kui:
                <ul>
                    <li>on alust arvata, et vallasasjas viibib isik, kellelt võib seaduse alusel võtta vabaduse või keda võib seaduse alusel läbi vaadata või kes on abitus seisundis</li>
                    <li>on alust arvata, et selles on asju, mida võib käesoleva seaduse või muu seaduse alusel võtta hoiule, hõivata või konfiskeerida</li>
                    <li>see on vajalik kaitstava isiku või valvatava objekti ohutuse tagamiseks</li>
                    <li>Kui valdaja isik on tuvastatav, tuleb teda esimesel võimalusel teavitada vallasasja läbivaatusest ning võimalusel isik koha peale kutsuda</li>
                </ul>
            </li>
        </ol>
        <h5 class="ml-2"><strong>Õiguste ette lugemin</strong></h5>
        <ol>
            <li>Õigused tuleb ette lugeda siis kui kahtlusalune autosse pannakse või kui olukord seda ei võimalda, siis jaoskonnas ühe esimese toiminguna</li>
            <li>"Teid kahtlustatakse {sisesta siia nimekiri kuritegudest}. Teil on õigus vaikida. Kõike mida te ütlete võidakse ja kasutataksegi teie vastu. Teil on õigus advokaadile alates X kuust.
                Kui te ei suuda endale advokaati palgata määrab selle teile võimalusel riik. On teile õigused arusaadavad?"</li>
        </ol>
    </div>
@endsection
