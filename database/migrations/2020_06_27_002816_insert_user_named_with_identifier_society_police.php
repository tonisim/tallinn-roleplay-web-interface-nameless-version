<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertUserNamedWithIdentifierSocietyPolice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')
            ->insert(
                array(
                    'identifier' => 'society_police',
                    'name' => 'Politsei Parkla',
                    'job' => 'unemployed',
                    'group' => 'user',
                    'firstname' => 'Konfiseeritud',
                    'lastname' => 'Auto'
                )
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
