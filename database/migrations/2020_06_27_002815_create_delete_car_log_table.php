<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeleteCarLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delete_car_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('confiscator');
            $table->string('confiscatorName');
            $table->string('carOwner');
            $table->string('carOwnerName');
            $table->string('carPlate');
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delete_car_log');
    }
}
