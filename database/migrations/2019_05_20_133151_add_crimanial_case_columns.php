<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCrimanialCaseColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('criminal_records', function (Blueprint $table) {
            $table->boolean('is_criminal_case');
            $table->timestamp('criminal_case_expires_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('criminal_records', function (Blueprint $table) {
            $table->dropColumn(['is_criminal_case', 'criminal_case_expires_at']);
        });
    }
}
