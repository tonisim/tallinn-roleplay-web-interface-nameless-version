<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMechanicReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mechanic_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_identifier');
            $table->string('job_type');
            $table->integer('job_price');
            $table->string('car_plate')->nullable();
            $table->string('service_type');
            $table->string('summary')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mechanic_reports');
    }
}
