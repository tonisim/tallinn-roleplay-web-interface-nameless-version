<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCriminalRecordLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('delete_criminal_record_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('characterName');
            $table->string('record');
            $table->string('deleter');
            $table->string('summary');
            $table->json('fine');
            $table->string('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    /*public function down()
    {
        Schema::dropIfExists('criminal_records');
    }*/
}
