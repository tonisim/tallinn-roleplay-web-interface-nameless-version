<?php

use App\Role;
use App\WebUser;
use Illuminate\Database\Seeder;

class WebUsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'admin')->first();
        $playerRole = Role::where('name', 'player')->first();
        $adminFiveMUser = \App\User::where('name', 'Aivar')->first();
        $playerFiveMUser = \App\User::where('name', 'Rass')->first();

        $user = new WebUser();
        $user->name = 'tonis';
        $user->email = 'tonisingvarmals@gmail.com';
        $user->password = bcrypt('Parool321');
        $user->save();
        $user->roles()->attach($adminRole);
        $user->fiveMUser()->attach($adminFiveMUser->identifier);

        $user = new WebUser();
        $user->name = 'player';
        $user->email = 'tonisingvarmals2@gmail.com';
        $user->password = bcrypt('Parool321');
        $user->save();
        $user->roles()->attach($playerRole);
        $user->fiveMUser()->attach($playerFiveMUser->identifier);
    }
}
