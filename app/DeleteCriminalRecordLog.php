<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeleteCriminalRecordLog extends Model
{
    protected $table = 'delete_criminal_record_log';
}
