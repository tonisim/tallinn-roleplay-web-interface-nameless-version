<?php

namespace App\Http\Controllers;


use App\Bill;

use App\Helpers\BillsHelper;
use App\Helpers\Sql\FivemFineTypes;
use App\Helpers\Sql\FivemUserHelper;
use App\Helpers\UserHelper;
use App\Helpers\WantedHelper;
use App\JobApplication;
use App\JobGrades;
use App\User;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class BailiffController extends Controller
{
    const ALLOWED_JOBS = [
        'taitur',
        'offtaitur'
    ];
    const ALLOWED_JOB_GRADES = [
        99
    ];

    public function allPoliceBills(Request $request)
    {

        $this->restrictAccess();

        return view(
            'bailiff.bills',
            BillsHelper::getBailiffSocietyBills('society_police'),
            ['bailiff_site_header' => 'Politsei Maksmata Arved TOP 5']

        );
    }public function allAmbulanceBills(Request $request)
    {

        $this->restrictAccess();

        return view(
            'bailiff.bills',
            BillsHelper::getBailiffSocietyBills('society_ambulance'),
            ['bailiff_site_header' => 'Kiirabi Maksmata Arved TOP 5']
        );
    }

    protected function restrictAccess()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job, self::ALLOWED_JOBS) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }

    protected function restrictAccessForCaptain()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job_grade, self::ALLOWED_JOB_GRADES) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }


}
