<?php
namespace App\Http\Controllers;

use App\Helpers\BillsHelper;
use App\JobGrades;
use App\User;
use Exception;

use App\DieselReports;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use PHPUnit\Framework\RiskyTestError;

/**
* Diesel controller
*/
class DieselController extends Controller
{

    /**
    * @var array
    */
    const ALLOWED_JOBS = [
        'diesel',
        'offdiesel'
    ];

    /**
    * @var array
    */
    const ALLOWED_JOB_GRADES = [
        99
    ];

    /**
    * Job types for diesel new report view
    * value => translation
    * @var array
    */
    const JOB_TYPE = [
        'carTransport' => 'diesel_car_trasnport',
        'carFix' => 'diesel_car_fix',
        'carTuning' => 'diesel_car_tuning',
        'carDeals' => 'diesel_car_deals',
    ];

    /**
    * Service types for diesel new report view
    * value/id => translation
    * @var array
    */
    const SERVICE_TYPE = [
        'npc' => 'dieselb_npc',
        'citizen' => 'dieselb_citizen',
        'ambulance' => 'dieselb_ambulance',
        'police' => 'dieselb_police',
        'policeCall' => 'dieselb_call',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function singleUser($steamId, $archive = false)
    {

        $this->restrictAccess();


    }

    public function showDieselWorkersList()
    {

        $this->restrictAccess();

        return view(
            'diesel.allWorkers',
            [
                'users' => User::where('job', '=', 'diesel')->orWhere('job', '=', 'offdiesel')->get(),
                'ranks' => JobGrades::where('job_name', '=', 'diesel')->get()
            ]
        );
    }


    public function newReport(Request $request)
    {

        $this->restrictAccess();
        return view(
            'diesel.newReport',
             [
                 'jobType' => self::JOB_TYPE,
                 'serviceType' => self::SERVICE_TYPE
             ]
         );
    }

    public function saveNewReport(Request $request)
    {

        try {
            $this->restrictAccess();
            $newReport = new DieselReports();
            $newReport->user_identifier = Auth::user()->fiveMUser()->first()->identifier;
            $newReport->job_type = $request->jobType;
            $newReport->job_price = $request->jobPrice;
            $newReport->car_plate = $request->carPlate;
            $newReport->service_type = $request->serviceType;
            $newReport->summary = $request->summary;
            $newReport->save();

            return redirect(route('dieselMyReports'));
        } catch (Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());

        }
    }

    public function myReports(Request $request)
    {
        try {
            $this->restrictAccess();
            return view(
                'diesel.myReports',
                 [
                     'reports' => Auth::user()->fivemUser()->first()->dieselReports
                 ]
             );
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());

        }
    }

    public function allReports(Request $request)
    {
        try {
            $this->restrictAccess();
            $this->restrictAccessForBoss();

            return view(
                'diesel.allReports',
                 [
                     'reports' => DieselReports::with('user')->orderBy('id', 'DESC')->paginate(20)
                 ]
             );
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage(), $exception->getCode());

        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allBills(Request $request)
    {

        $this->restrictAccess();

        return view(
            'common.bills',
            BillsHelper::getSocietyBills('society_diesel')
        );
    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    protected function restrictAccess()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job, self::ALLOWED_JOBS) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    protected function restrictAccessForBoss()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job_grade, self::ALLOWED_JOB_GRADES) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }
}
