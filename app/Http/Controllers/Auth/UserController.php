<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\WebUser;
use App\WhitelistApplications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 * @package App\Http\Controllers\Auth
 */
class UserController extends Controller
{

    public function create()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {

        request()->validate([

            'name' => 'required|min:2|max:50',

            'email' => 'required|email|unique:web_users',

            'password' => 'required|min:6|confirmed',
            
            'fiveMUser' => 'required|string|min:15|max:15'

        ], [

            'name.required' => 'Name is required',

            'name.min' => 'Name must be at least 2 characters.',

            'name.max' => 'Name should not be greater than 50 characters.',

            'fiveMUser.min' => __('wlValidationErrors.hex_min'),

            'fiveMUser.max' => __('wlValidationErrors.hex_max'),

        ]);
        $steamId = 'steam:' . strtolower($request->fiveMUser);
        error_log($steamId);
        $fiveMUserId = DB::table('whitelist')
            ->where('identifier', '=',  $steamId)
            ->get(['identifier']);

        if (!$fiveMUserId->count()) {
            return redirect()->back()->withErrors(['fiveMUser' => ['message' => 'Sellist kasutajat pole whitelistis.Mine loo endale Whitelist Whitelist avalduse kaudu.']])
                ->withInput(\request()->all());
        } elseif ($fiveMUserId->count() > 1) {
            return redirect()->back()->withErrors(
                ['fiveMUser' =>
                    [
                        'message' => 'There are more then 1 FiveM user named ' . \request('fiveMUser')
                            . '. Please change your FiveM/Steam name, login into gama and come back to finish your user creation.'
                    ]
                ]
            )->withInput(\request()->all());
        }

        if (
            DB::table('user_web_user')
                ->where('user_identifier', '=', $fiveMUserId->first()->identifier)
                ->get()
                ->count()
        ) {
            return redirect()->back()->withErrors(
                ['fiveMUser' =>
                    [
                        'message' => 'Sellise HEXiga konto juba eksisteerib.'
                    ]
                ]
            )->withInput(\request()->all());
        }


        $user = WebUser::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(\request('password')),
        ]);

        $user
            ->roles()
            ->attach(Role::where('name', 'player')->first());
        $user->fiveMUser()->attach($fiveMUserId->first()->identifier);
        error_log($user->id);


        WhitelistApplications::create([
            'user_identifier' => $fiveMUserId->first()->identifier,
            'steamName' => request('name'),
            'characterName' => 'Created by Register',
            'isAccepted' => 1,
            'isRejected' => 0,
            'web_user_id'=> $user->id,
            'extra_questions' =>'{"age":"23","discord":"-","biography_description":"-","rprules":"-","situation":"-"}'
        ]);


        error_log(request('fiveMUser'));
        Auth::loginUsingId($user->id);
        return redirect('/home');




    }
}
