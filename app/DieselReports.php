<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne as HasOne;

class DieselReports extends Model
{

    /**
     * @return HasOne
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }
}
