<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsTo;

class ProfilePicture extends Model
{

    /**
     * @return BelongsTo
     */
    public function webUser()
    {

        return $this->belongsTo(WebUser::class);
    }
}
