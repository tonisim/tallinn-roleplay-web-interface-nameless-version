<?php
namespace App\Helpers;

/**
 * Class SkillsHelper
 * @package App\Helpers
 */
class SkillsHelper
{

    /**
     * @param int $amount
     * @return int|float
     */
    public static function getPercentOf($amount)
    {

        if(!$amount){
            return 0;
        }
        $percent =$amount / 100;
        return number_format( $percent * 100, 2 );
    }
}
