<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhitelistApplications extends  Model
{
    protected $fillable = [
        'user_identifier', 'steamName', 'characterName', 'isAccepted', 'isRejected', 'rejectionReason', 'web_user_id', 'extra_questions',
    ];

    public function webUser()
    {
        return $this->belongsTo(WebUser::class);
    }

    public function user()
    {

        return $this->belongsTo(User::class);

    }

}
