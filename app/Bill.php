<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{

    protected $fillable = [
        'identifier', 'sender', 'target_type', 'target', 'label', 'amount',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'billing';
}
