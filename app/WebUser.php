<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne as HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class WebUser extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'web_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return BelongsToMany
     */
    public function fiveMUser()
    {

        return $this->belongsToMany(User::class);
    }

    /**
     * @return mixed
     */
    public function getFiveMUserData()
    {

        return $this->fiveMUser()->first();
    }

    /**
     * @return HasOne
     */
    public function whitelistApplication()
    {

        return $this->hasOne(WhitelistApplications::class);
    }

    /**
     * @return HasMany
     */
    public function jobApplication()
    {

        return $this->hasMany(JobApplication::class);
    }

    /**
     * @return HasOne
     */
    public function profilePicture()
    {

        return $this->hasOne(ProfilePicture::class);
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles)

    {

        if (is_array($roles)) {

            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');

        }

        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');

    }

    /**
     * Check multiple roles
     *
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles)

    {

        return null !== $this->roles()->whereIn('name', $roles)->first();

    }

    /**
     * Check one role
     *
     * @param string $role
     * @return bool
     */
    public function hasRole($role)

    {

        return null !== $this->roles()->where('name', $role)->first();

    }
}
